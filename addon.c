/*
   This file is NOT BUILT by default.  It provides an example of how
   to add new builtins to rc.

   To define a new builtin, it must appear in the macro ADDONS, which
   is a comma-separated list of pairs of function pointers (the
   implementation of the new builtin) and string literals (the name of
   the new builtin).

   Any new builtin functions must have prototypes of the same form:

	void b_NAME(char **av);

   The first argument, av[0], is the name of the builtin.  The last
   argument is followed by a NULL pointer.

   Builtins report their exit status using set(TRUE) or set(FALSE).

*/

#include <fcntl.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>

void b_sum(char **av)
{
	long sum = 0;

	while (*++av)
		sum += atol(*av);
	fprint(1, "%ld\n", sum);
	set(TRUE);
}

void b_prod(char **av)
{
	long sum = 1;

	while (*++av)
		sum *= atol(*av);
	fprint(1, "%ld\n", sum);
	set(TRUE);
}

void b_syd_stat(char **av)
{
	char path[PATH_MAX] = { 0 };
	struct stat buf;

	strncat(path, "/dev/syd", sizeof(path) - 1);
	if (*av && *(av + 1)) {
		strncat(path, "/", sizeof(path) - 2);
		while (*++av)
			strncat(path, *av, sizeof(path) - strlen(path) - 1);
	}

	fprint(2, "casting magic %s: ", path);
	if (lstat(path, &buf) == -1) {
		perror("");
		set(FALSE);
	} else {
		fprint(2, "ok!\n");
		set(TRUE);
	}
}

#define ADDONS \
	{ b_sum, "+" }, \
	{ b_prod, "x" }, \
	{ b_syd_stat, "s" },
